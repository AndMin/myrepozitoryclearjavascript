
var a = 21;

var b = String(a);
console.log(b); //"21"

// Преобразование в логические типы
b = !!b;
console.log(b);

b = Boolean(b);
console.log(b); //true


// Преобразование String в число

b = String(a);
b = Number(b);
console.log(b); //21

b = String(a);
b = parseInt(a);
console.log(b); //String преобразований к числу через функцию parneInt

b = String(a);
b = +a;
console.log(b); //String преобразований к числу через унарный оператор +
