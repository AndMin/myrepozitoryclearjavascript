    var total = 0;
    var resultId = document.getElementById("result"); 
	var first;
	var	second;
	
	function getRndNumber(){
    return Math.floor((Math.random() * 6) + 1);
    }
	
	function showTextResult(text){
		resultId.innerHTML += text;
	}
	
	
	function isNumbersEqual(){
		 if (first == second){
        var number = first;
        showTextResult ("Выпал дубль. Число " + number + "<br>");
		}
    }
	function isBigDifference(first, second){
		if (first < 3 && second > 4){
        var diff = second - first;
        showTextResult ("Большой разброс между костями. Разница составляет " + diff + "<br>");
		}
	}
	
	function getResult(total){
		var result = (total > 100) ? ("Победа, вы набрали " + total + " очков" + "<br>"):("Вы проиграли, у вас " + total + " очков" + "<br>")
		showTextResult(result);
	}

function run(){
	for (var i = 1; i <= 15; i++){
		if (i == 8 || i == 13){
        continue;
		}
		first = getRndNumber();
		second = getRndNumber();
		
		showTextResult("Первая кость: " + first + " Вторая кость: " + second + "<br>");
		
		isNumbersEqual(first, second);
		isBigDifference(first, second);
		
		total += first + second; 
		
	}
		
		 getResult(total);
}

	run();
